# HELIX SPREADSHEET TASK

It was a long time I last time used `python` and I have never mastered it. I am sure a lot things can be expressed better using more `pythonic` idioms, but nevertheless I am pretty sure the core logic of the program is quite robust.
It took me about 6.5 hours to prepare the solution and while being interrupted by other duties in meantime.

I used `python2.7` as interpreter. 

## Code Structure

There are two modules `calculator` and `spreadsheet`.

The solely responsibility of `calculator` is to take care of expressions in postfix-notation. An expression is a `list` of strings (stripped from any withe spaces) -- aka `tokens`. Calculator can classify each token as number, operator, reference or invalid. Expression with invalid token inside is regarded as invalid itself. Calculator can test if an expression is valid from syntactic perspective as well -- in other words, if all references are resolved (substituted with a proper value) then the expression itself can be correctly computed.

Class `SpreadSheet` keeps track of all cells. It makes a heavy use of pythonian `dictionaries` (rows, columns and cells are all represent as dictionaries).

*Main logic works as follows:*

 1. Constructor of the class is passed a `path` to a `csv` file.
 2. The file is read and cells created with raw strings as expressions.
 3. Cells are initialised and classified as erroneous (incorrect expression), active (depends on external cells) or ready (can be calculated straight away). Instance of the `spreadsheet` keeps track of all active cells; each active cell keeps track of the cells that it depends on.
 4. There are 10 iterations (like in angular digest cycle:)), after which the system should resolve all dependencies and stabilised. Each active cell checks if the cells it references have changed their state and it reacts accordingly:

  * if any of referenced cell became erroneous one then the cell becomes erroneous as well
  * if a referenced cell computed its value, then the value is substituted in the expression and the referenced cell is removed from being tracked
  * if there are no more referenced cell that are tracked the cell calculates its value.
 5. All cells that did **not** stabilise after 10 iterations are regarded as erroneous -- they are very likely involved in some circular dependency.

 
## Tests

I used some very basic functionality of `unittest` module.

### Calculator-module

Run command: `python -m unittest -v calculatorUT`

I created some generator for creating correct postfix-notation expressions. Moreover, substituting all operators with `+` (or `*`) it is possible to easily calculate correct result (for long expressions and due to float precision the results may diverge though).

### Spreadsheet-module

Run command: `python -m unittest -v spreadsheetUT`

It loads csv files located in `test/spreadsheets` and checks for corner cases (described in `TestSpreadSheet.testDescriptions`). Some random tests would be really welcomed, but I think it is too much for this simple task (especially as they are quite time consuming to prepare).

## Limitations

Right now, only columns from range 'A'-'Z' are supported -- having a wider row raises an exception. (if needed can implement it tomorrow)

I do not consider precision of floating point numbers.

## Trade-offs

 1. propagation of changes in active cells could be made more computationally efficient by making the dependency graph undirected, but it would increase the memory requirements and complexity of the program
 2. to fully test the logic some proper randomised test would be needed, but creating them would be much harder than solving the task at hand :)
  

import re
from numpy import float64, inf

# http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

TOKEN_TYPE = enum('NUMBER', 'REFERENCE', 'OPERATOR', 'INVALID')

refPattern = re.compile("^[a-zA-Z]+[0-9]+$")

def isFloat(value):
    try:
        float64(value)
        return True
    except ValueError:
        return False

def getTokenType(token):

    if isFloat(token):
        return TOKEN_TYPE.NUMBER
    elif token == "+" or token == "-" or token == "*" or token == "/":
        return TOKEN_TYPE.OPERATOR
    elif refPattern.match(token):
        return TOKEN_TYPE.REFERENCE
    else:
        return TOKEN_TYPE.INVALID

def isTokenValid(token):
    return getTokenType(token) != TOKEN_TYPE.INVALID

# expression is a list of strings/tokens; all whitespaces stripped
# checks if each token is a valid one
def areExpressionTokensValid(expression):
    for token in expression:
        if not isTokenValid(token):
            return False
    return True

# uses push-down automaton to parse the expression
def isExpressionSyntaxValid(expression):
    if areExpressionTokensValid(expression):
        stack = []
        for token in expression:
            type = getTokenType(token)
            if type == TOKEN_TYPE.NUMBER:
                stack.append(token)
            elif type == TOKEN_TYPE.REFERENCE:
                # convert reference to a numeric value,
                # as correct reference is going to be substitute by a numeric value
                stack.append(1)
            elif type == TOKEN_TYPE.INVALID:
                return False # invalid token => invalid expression
            elif type == TOKEN_TYPE.OPERATOR:
                if stack.__len__() >= 2:
                    stack.pop()
                    stack.pop()
                    # mock-up operation by return a numeric value
                    stack.append(1)
                else:
                    # at least 2 operands needed
                    return False
            else:
                raise Exception("Unknown token type: " + token)
        # after all operations there should be one element on the stack (result)
        # for sure it is a number, as only numbers were pushed
        return stack.__len__() == 1
    else:
        return False

# it is assumed that expression is syntactically correct
# no tokens of type REFERENCE or INVALID should be present
def calculate(expression):
    stack = []
    for token in expression:
        type = getTokenType(token)
        if type == TOKEN_TYPE.NUMBER:
            stack.append(float64(token))
        elif type == TOKEN_TYPE.OPERATOR:
            op2 = stack.pop()
            op1 = stack.pop()
            if token == '+':
                result = op1 + op2
            elif token == '-':
                result = op1 - op2
            elif token == '*':
                result = op1 * op2
            elif token == '/':
                if op2 == 0:
                    result = inf * op1
                else:
                    result = op1 / op2
            else:
                raise Exception("Unknown token: " + token)
            stack.append(result)
        else:
            raise Exception("Unknown token type: " + token)
    return stack.pop()

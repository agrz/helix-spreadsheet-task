import csv
import sys
from calculator import *

CELL_STATE = enum("ACTIVE", "ERROR", "READY")

class SpreadSheet:

    ERR_MSG = "#ERR"
    REF_MSG = "#REF"

    EXPRESSION_KEY = "E"
    STATE_KEY = "S"
    VALUE_KEY = "V"
    DEPENDS_UPON_KEY = "D"

    # number of iterations
    ITERATIONS_COUNT = 10

    def __init__(self, fileName):
        self.activeCells = []
        self.readCSV(fileName)
        self.initCells()
        self.calculateReferences()

    def getFinalValue(self, cell):
        if cell[SpreadSheet.STATE_KEY] == CELL_STATE.READY:
            value = cell[SpreadSheet.VALUE_KEY]
        elif cell[SpreadSheet.STATE_KEY] == CELL_STATE.ERROR:
            value = SpreadSheet.ERR_MSG
        else:
            value = SpreadSheet.REF_MSG
        return value

    def printCells(self):
        for row in self.cells:
            line = ""
            for col in self.cells[row]:
                cell = self.cells[row][col]
                line += str(self.getFinalValue(cell)) + ", "
            print line[:-2], "\n"


    def getFinalValues(self):
        result = {}
        for row in self.cells.keys():
            result[row] = {}
            for col in self.cells[row].keys():
                result[row][col] = self.getFinalValue(self.cells[row][col])
        return result

    def readCSV(self, fileName):
        r = 0
        self.cells = {}
        with open(fileName, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                r += 1
                c = 0
                self.cells[r] = {}
                for col in row:
                    c += 1
                    if (c + ord('A')) > ord('Z'):
                        raise Exception("Only supporting columns up to Z")
                    self.cells[r][c] = {
                        SpreadSheet.EXPRESSION_KEY: col
                    }

    # for now assume single letter in the token -- columns from A-Z
    # if address outside of initialized cells return None
    def getCell(self, referenceToken):
        col = ord(referenceToken[0])+1 - ord('A')
        row = int(referenceToken[1:])
        if row in self.cells.keys():
            if col in self.cells[row].keys():
                r = self.cells[row][col]
            else:
                r = None
        else:
            r = None
        return r

    def setCellToError(self, cell):
        cell[SpreadSheet.STATE_KEY] = CELL_STATE.ERROR
        del cell[SpreadSheet.EXPRESSION_KEY]
        if SpreadSheet.DEPENDS_UPON_KEY in cell.keys():
            del cell[SpreadSheet.DEPENDS_UPON_KEY]

    def calculateCell(self, cell):
        cell[SpreadSheet.STATE_KEY] = CELL_STATE.READY
        cell[SpreadSheet.VALUE_KEY] = calculate(cell[SpreadSheet.EXPRESSION_KEY])
        del cell[SpreadSheet.EXPRESSION_KEY]
        if SpreadSheet.DEPENDS_UPON_KEY in cell.keys():
            del cell[SpreadSheet.DEPENDS_UPON_KEY]

    # put cell into 1 from 3 states:
    # a) READY -- if expression can be calculated right away
    # b) ERROR -- if expression is syntactically incorrect
    # c) ACTIVE -- if expression depends on other
    def initCell(self, cell):
        cell[SpreadSheet.EXPRESSION_KEY] = cell[SpreadSheet.EXPRESSION_KEY].strip().split()
        # empty but defined in csv cells equal to 0
        if len(cell[SpreadSheet.EXPRESSION_KEY]) == 0:
            cell[SpreadSheet.EXPRESSION_KEY] = [0]
        if isExpressionSyntaxValid(cell[SpreadSheet.EXPRESSION_KEY]):
            dependsUpon = []
            # normilize tokens and extract referenced cells
            for ndx, member in enumerate(cell[SpreadSheet.EXPRESSION_KEY]):
                if getTokenType(member) == TOKEN_TYPE.REFERENCE:
                    cell[SpreadSheet.EXPRESSION_KEY][ndx] = member.upper()
                    externalCell = self.getCell(cell[SpreadSheet.EXPRESSION_KEY][ndx])
                    # wrong reference
                    if externalCell == None:
                        self.setCellToError(cell)
                        return
                    dependsUpon.append((ndx, externalCell))
            # no external dependencies and valid syntax -- can calculate right away
            if len(dependsUpon) == 0:
                cell[SpreadSheet.STATE_KEY] = CELL_STATE.READY
                cell[SpreadSheet.VALUE_KEY] = calculate(cell[SpreadSheet.EXPRESSION_KEY])
                del cell[SpreadSheet.EXPRESSION_KEY]
            else:
                cell[SpreadSheet.STATE_KEY] = CELL_STATE.ACTIVE
                cell[SpreadSheet.DEPENDS_UPON_KEY] = dependsUpon
                self.activeCells.append(cell)
        else:
            self.setCellToError(cell)

    def initCells(self):
        for row in self.cells.keys():
            for col in self.cells[row].keys():
                self.initCell(self.cells[row][col])

    def handleSingleActiveCell(self, cell):
        calculated = []
        for external in cell[SpreadSheet.DEPENDS_UPON_KEY]:
            if external[1][SpreadSheet.STATE_KEY] == CELL_STATE.READY:
                calculated.append(external)
            elif external[1][SpreadSheet.STATE_KEY] == CELL_STATE.ERROR:
                self.setCellToError(cell)
                return
        for ready in calculated:
            cell[SpreadSheet.DEPENDS_UPON_KEY].remove(ready)
            ndx, external = ready
            cell[SpreadSheet.EXPRESSION_KEY][ndx] = external[SpreadSheet.VALUE_KEY]
        if len(cell[SpreadSheet.DEPENDS_UPON_KEY]) == 0:
            self.calculateCell(cell)
            self.activeCells.remove(cell)

    def calculateReferences(self):
        for i in range(0, SpreadSheet.ITERATIONS_COUNT):
            for active in self.activeCells:
                self.handleSingleActiveCell(active)
            if len(self.activeCells) == 0:
                return
        # these which have not stabilized are treated as erroneous
        for active in self.activeCells:
            self.setCellToError(active)

if __name__ == "__main__":
    spreadsheet = SpreadSheet(sys.argv[1])
    spreadsheet.printCells()
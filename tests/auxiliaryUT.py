import sys
import csv
sys.path.append('../src')

from calculator import *
import random
from numpy import float64

EXPRESSION_KEY = 'E'
RESULT_KEY = 'R'

def compareToken(token, correctType):
    return getTokenType(token) == correctType

def getRandomOperator():
    r = random.randint(0, 3)
    if r == 0:
        return "+"
    if r == 1:
        return "-"
    if r == 2:
        return "*"
    if r == 3:
        return "/"

def generateCorrectExpression(iterations, probability, min, max):
    exp = [random.randint(min, max)]
    for i in range(0, iterations):
        for j in range(0, len(exp)):
            token = exp.pop(j)
            if getTokenType(token) == TOKEN_TYPE.NUMBER:
                if (random.random() <= probability):
                    exp.insert(j, getRandomOperator())
                    exp.insert(j, random.randint(min, max))
            exp.insert(j, token)
    return exp

def getRandomToken(min, max):
    if random.random() < 0.7:
        return random.randint(min, max)
    else:
        return getRandomOperator()


generatedCorrectExpressions = []
for i in range(0, 10):
    generatedCorrectExpressions.append(generateCorrectExpression(6, 0.5, -10, 10))

correctExpressions = [
    {
        EXPRESSION_KEY: ['1'],
        RESULT_KEY: 1
    },
    {
        EXPRESSION_KEY: ['1', '1', '+'],
        RESULT_KEY: 2
    },
    {
        EXPRESSION_KEY: ['2', '1', '1', '+', '*'],
        RESULT_KEY: 4
    },
    {
        EXPRESSION_KEY: ['2', '2', '/', '1', '1', '+', '*'],
        RESULT_KEY: 2
    }
]


def convertCorrectExpression(exp, startValue, operatorFn, operatorSign):
    copy = list(exp)
    result = startValue
    for ndx, member in enumerate(copy):
        type = getTokenType(member)
        if type == TOKEN_TYPE.NUMBER:
            result = operatorFn(result, float64(member))
        elif type == TOKEN_TYPE.OPERATOR:
            copy[ndx] = operatorSign
        else:
            raise "Unknown type"
    return {EXPRESSION_KEY: copy, RESULT_KEY: result}

def add(x,y): return x+y
def mult(x,y): return x*y

for exp in generatedCorrectExpressions:
    correctExpressions.append(convertCorrectExpression(exp, 0, add, "+"))
    correctExpressions.append(convertCorrectExpression(exp, 1, mult, "*"))


incorrectExpressions = []

for exp in generatedCorrectExpressions:
    incorrect = list(exp)
    incorrect.insert(random.randint(-1, len(exp)), getRandomToken(-10, 10))
    incorrectExpressions.append(incorrect)


def readCSV(fileName):
    r = 0
    cells = {}
    with open(fileName, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            r += 1
            c = 0
            cells[r] = {}
            for col in row:
                c += 1
                cells[r][c] = col.strip()
                if isFloat(cells[r][c]):
                    cells[r][c] = float64(cells[r][c])
    return cells

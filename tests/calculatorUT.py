import sys
sys.path.append('../src')

import unittest
from auxiliaryUT import *
from calculator import *


class TestCalculator(unittest.TestCase):

    def testTokenTypes(self):
        # OPERATOR
        self.failUnless(compareToken("+", TOKEN_TYPE.OPERATOR))
        self.failUnless(compareToken("-", TOKEN_TYPE.OPERATOR))
        self.failUnless(compareToken("*", TOKEN_TYPE.OPERATOR))
        self.failUnless(compareToken("/", TOKEN_TYPE.OPERATOR))
        self.failUnless(compareToken("/", TOKEN_TYPE.OPERATOR))
        # NUMBER
        self.failUnless(compareToken("0", TOKEN_TYPE.NUMBER))
        self.failUnless(compareToken("1", TOKEN_TYPE.NUMBER))
        self.failUnless(compareToken("1000", TOKEN_TYPE.NUMBER))
        self.failUnless(compareToken("1000.12", TOKEN_TYPE.NUMBER))
        # REFERENCE
        self.failUnless(compareToken("A1", TOKEN_TYPE.REFERENCE))
        self.failUnless(compareToken("b2", TOKEN_TYPE.REFERENCE))
        self.failUnless(compareToken("aa11", TOKEN_TYPE.REFERENCE))
        self.failUnless(compareToken("bbb123", TOKEN_TYPE.REFERENCE))
        # INVALID
        self.failUnless(compareToken("a1000.12.a", TOKEN_TYPE.INVALID))
        self.failUnless(compareToken("12a", TOKEN_TYPE.INVALID))
        self.failUnless(compareToken("a", TOKEN_TYPE.INVALID))
        self.failUnless(compareToken("", TOKEN_TYPE.INVALID))
        self.failUnless(compareToken("a12a", TOKEN_TYPE.INVALID))

    def testTokenValid(self):
        self.failUnless(isTokenValid("+"))
        self.failUnless(isTokenValid("11"))
        self.failUnless(isTokenValid("A12"))
        self.failIf(isTokenValid("2b"))
        self.failIf(isTokenValid(""))

    def testExpressionTokens(self):
        for exp in generatedCorrectExpressions:
            self.failUnless(areExpressionTokensValid(exp), exp)
        for exp in correctExpressions:
            self.failUnless(areExpressionTokensValid(exp[EXPRESSION_KEY]), exp[EXPRESSION_KEY])
        for exp in incorrectExpressions:
            self.failUnless(areExpressionTokensValid(exp), exp)
        self.failUnless(areExpressionTokensValid(['1', '1', '+']))
        self.failUnless(areExpressionTokensValid(['1', '1', '+', '-', '12', 'a12', '12.11']))
        self.failIf(areExpressionTokensValid(['1', '1', '+', '-', 'a', '12', 'a12', '12.11']))
        self.failIf(areExpressionTokensValid(['1', '1', '+', '-', 'a', '12', 'a12', '12.11', 'bc']))

    def testExpressionSyntax(self):
        for exp in generatedCorrectExpressions:
            self.failUnless(isExpressionSyntaxValid(exp), exp)
        for exp in correctExpressions:
            self.failUnless(isExpressionSyntaxValid(exp[EXPRESSION_KEY]), exp[EXPRESSION_KEY])
        for exp in incorrectExpressions:
            self.failIf(isExpressionSyntaxValid(exp), exp)

    def testCalculate(self):
        for exp in correctExpressions:
            result = calculate(exp[EXPRESSION_KEY])
            self.failUnless(result == exp[RESULT_KEY], result - exp[RESULT_KEY])


if __name__ == '__main__':
    unittest.main()
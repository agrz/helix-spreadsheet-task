import sys
sys.path.append('../src')

import unittest
from auxiliaryUT import *
from spreadsheet import *

def compare(spreadsheet, result):
    try:
        cells = spreadsheet.getFinalValues()
        for row in cells.keys():
            for col in cells[row].keys():
                if cells[row][col] != result[row][col]:
                    print cells[row][col], result[row][col], type(cells[row][col]), type(result[row][col])
                    return False
        return True
    except Exception as err:
        print err
        return False

def createOutsideZ():
    return SpreadSheet("./spreadsheets/5.csv")

class TestSpreadSheet(unittest.TestCase):

    testDescriptions = {
        1: "ORIGINAL",
        2: "REFERENCE OUTSIDE",
        3: "CIRCULAR REFERENCE",
        4: "DIVIDING BY ZERO",
        5: "OUTSIDE 'Z'"
    }

    def testCSV(self):
        for i in range(1, 5):
            spreadsheet = SpreadSheet("./spreadsheets/" + str(i) + ".csv")
            result = readCSV("./spreadsheets/" + str(i) + "_result.csv")
            self.failUnless(compare(spreadsheet, result), TestSpreadSheet.testDescriptions[i])

    def testOutside(self):
        self.assertRaises(Exception, createOutsideZ)

if __name__ == '__main__':
    unittest.main()